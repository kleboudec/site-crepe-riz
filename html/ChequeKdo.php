<head>
  <meta name="author" content="PIERRE Gaëtan">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/video.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="../css/contact.css">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script src="ressources/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <title>Chèque Cadeaux</title>
</head>

<body>
<?php
include_once "menuNav.php";
?>
<div id="main">
  <main class="container lex-shrink-0" style="text-align:justify">
    <div class="row">
      <div class="col-md-12">

        <form id="contact" action="" method="post">
          <h3>Formulaire de réservation de chèque Cadeaux</h3>
          <fieldset>
            <input placeholder="Votre nom" type="text" tabindex="1" required autofocus>
          </fieldset>
          <fieldset>
            <input placeholder="Votre adresse mail" type="email" tabindex="2" required>
          </fieldset>
          <fieldset>
            <input placeholder="Votre téléphone (optionnel)" type="tel" tabindex="3" required>
          </fieldset>
          <fieldset>
            <select name="pets" type ="text" id="pet-select">
              <option value="">--Veuillez choisir une sommes--</option>
              <option value="5">5€</option>
              <option value="10">10€</option>
              <option value="15">15€</option>
              <option value="20">20€</option>
              <option value="50">50€</option>
              <option value="100">100€</option>
            </select>
          </fieldset>
          <fieldset>
            <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Réserver</button>
          </fieldset>
        </form>
      </div>
    </div>
  </main>
