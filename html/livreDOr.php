<!DOCTYPE html>
<div>
<head>
    <title>Livre d'or</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
    <body>
    <?php
    include_once "menuNav.php";
    ?>
    <div id="main">
    <?php
    $host = 'localhost';
  	$user = 'root';
  	$pass = '';
    $bdd = 'creperiz';
    $table = 'guestbook';
    $url = $_SERVER['PHP_SELF'];
    $mysqli = new mysqli("localhost", "root", "", "creperiz");
    if(isset($_POST["ajout"])) {
        $ajout = $_POST["ajout"];
        $name = $_POST["name"];
        $lemail = $_POST["lemail"];
        $comment = $_POST["comment"];
        $comment=str_replace("'","\'",$comment);
    }
    else{
        $ajout=false;
    }

    // Si le formulaire à été soumis, $ajout vaudra true
    if ($ajout) {
        if (!empty($name) && !empty($lemail) && !empty($comment)) {
            // Définit la variable de date
            $date = time();

    // Créé la requête
            $query = "INSERT INTO $table(date,name,email,comment) VALUES('$date','$name','$lemail','$comment')";

     // Exécute la requête d'insertion du message
     $result = mysqli_query($mysqli,$query) or die('Erreur SQL : '.mysqli_error($mysqli));
	 } else {
   // On affiche un message d'erreur
     echo '<script >alert(\'Merci de remplir tout les champs.\');location.href=\''.$_SERVER['HTTP_REFERER'].'\';</script>';
	 }
    }
?>
    <table width="100%" height="100%" ><tr><td>
  	     <b>
  	     <?php
  	     // Requête ordonnant les messages par dates
 	     $query = "SELECT date,name,email,comment FROM $table ORDER BY date DESC";

  	     // Exécute la requête précédente
  	     $result = mysqli_query($mysqli,$query) or die('Erreur SQL : '.mysqli_error());

  	     // Renvoie le nombre de lignes pour pouvoir afficher le nombre total de messages postés
  	     echo mysqli_num_rows($result);
  	     ?>
     	     messages
     	     </b>
     	     <table width="95%"><tr><td>
                        <hr size=1>

              	     <?php
                	     // Définit la boucle : tant qu'il y a des messages dans la BDD
  	     while ($val = mysqli_fetch_array($result)) {
                      echo '<font color="black" face="verdana, arial">Message de :</font>';
  	         // Affiche le pseudo du posteur de message (avec lien mailto:)
  	         echo '<a href="mailto:'.$val['email'].'">'.$val['name'].'</a>';
  	         echo '<font color="black" face="verdana, arial">Posté le : </font>';
  	         // Affiche la date où a eté posté le message
 	         echo '<font color="orange">'.date("d/m/Y",$val['date']).'</font>';
  	         // Affiche le message posté
  	         echo '<div>'.htmlentities($val['comment']).'</div>';
  	         echo '<hr size=1>';
  	     }
  	     ?>
                </td></tr></table>
     	 </td></tr></table>

    <table width="80%"><tr><td>
                  <form method="post" action="<?php echo $url; ?>" onSubmit="return testform(this.name,this.lemail,this.comment)">
                      <input type="hidden" name="ajout" value="true">
                    	     <table cellspacing="0" cellpadding="0" style="border: 1px solid white;" height="288">
                     	         <tr>
                          	             <td width="130"><font color="black">Nom</font></td>
                         	             <td width="205"><input name="name" size="20"></td>
                         	         </tr>
                      	         <tr>
                                         <td width="130"><font color="black">Votre Email :</font></td>
                                     <td width="205"><font color="black"><input name="lemail" size="20"></font></td>
                       	         </tr>
                      	         <tr>
                       	             <td colspan="2">
                               	                 <textarea name="comment" rows="4" cols="55"></textarea>
                                              <br><br>
                            	                 <input type="Submit" value="Valider">
                            	                 <br><br>
                              	                 <a href="#">Haut</a><br>
                                	             </td>
                           	         </tr>
                         	     </table>
                     	     </form>
               	 </td></tr></table>



    <?php
    mysqli_close($mysqli);
    ?>

    </body>

<script language="JavaScript">
    <!--
    function verif(lemail) {
        var arobase = lemail.indexOf("@"); var point = lemail.lastIndexOf(".")
        if((arobase < 3)||(point + 2 > lemail.length)||(point < arobase+3))
            return false
        return true
    }

    function testform(lenom,mail,comment) {
        if(lenom.value=="") {
            lenom.focus();
            return false
        }
        if(!verif(mail.value)) {
            mail.value="";
            mail.focus();
            return false
        }
        return true
    }
    //-->
</script>

</div>



</html>