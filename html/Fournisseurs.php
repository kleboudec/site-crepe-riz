<!DOCTYPE html>
<html>
<head>
    <title>Breizh Ton Riz</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>
<body>
<?php
include_once "menuNav.php";
?>
<div id="main">
	<h4 style="text-align: center;"><b>Nos Fournisseurs</b></h4>
	<div style="display: block; text-align: center; font-size: 20">
		Aubert le grand laitier (Lait) : <a href="https://www.agrilait.fr"/>Lait</a><br>
		<img src="../src/agrilait.png" height=100 px width=200 px/><br>
		Merlin Graddle (Oeufs): <a href="https://www.daucyfoodservice.com/produits_cat/oeufs/">Oeufs</a><br>
		<img src="../src/daucy_foodservice.png" height=100 px width=200 px><br>
		Gaëtan le plus beau (Farine) : <a href="https://www.c3ao.fr/fournisseurs/">Farine</a><br>
		<img src="../src/c3o.jpg" height=100 px width=200 px/>
	</div>
</div>
</body>
</html>

