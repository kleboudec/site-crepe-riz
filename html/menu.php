<html>
    <div style="margin-left:300px;margin-right: 300px;">
    <h1 style="text-align:center">Menu</h1>
        <hr>
    <h2 style="text-align:left;">Menu Galette <span style="float :right">15,50€</span></h2>
    <p>Une galette trio ou galette<br>
    1 petite salade d'accompagnement<br>
    1 boisson de votre choix <br>
    Une crepe traditionnelle</p>

    <h2 style="text-align:left;">Menu Galette Salade <span style="float :right">16,50€</span></h2>
        <p>1 grosse salade de crudités servie avec une galette de sarrasin froide<br>
            1 boisson de votre choix<br>
            Bière avec 1.50€ de supplément<br>
            1 crêpe traditionnelle ou crêpe du jour</p>


        <h2>Customisez votre menu avec pour 1,50€ de plus</h2>
        <p>Au choix</p>
        <p>1 ingrédient supplémentaire dans votre galette<br>
            1 galette suggestion<br>
            1 crêpe suggestion<br>
            1 bière Bretonne<br>
            De la chantilly sur votre crêpe dessert<br>
            1 boisson chaude en fin de repas</p>
        <h2 style="text-align:left;">Menu Wrap Crudités Thon <span style="float :right">12,50€</span></h2>
        <p>
            1 Wrap Crudités Thon<br>
            1 Taboulé de Sarrasin Grillé<br>
            1 boisson 33cl ou eau 50cl</p>
        <hr>
        <h2 style="text-align:center;">Les galettes de Sarrasin </h2>
        <p>100 % blé noir / sans gluten</p>
        
        <h2 style="text-align:left;">Nos suggestions <span style="float :right">9,50€</span></h2>

        <h3>Super complète</h3>
        <p>Œuf, jambon, emmental, champignons</p>

        <h3>Piou piou</h3>
        <p>Poulet, crème fraîche, emmental, oignons poêlés</p>

        <h3>Coup de soleil</h3>
        <p>Jambon de pays, tomates séchées, œuf, échalotes séchées</p>

        <h3>Eliot ness</h3>
        <p>Œuf brouillé, cheddar, bacon, oignons frits croustillants</p>

        <h3>Saint-Malo</h3>
        <p>Œuf, tombée de poireaux, saucisse, sauce moutarde</p>

        <h3>Jardinière</h3>
        <p>Oignons poêlés, courgettes grillées, champignons, graines</p>

        <h3>Sucrée-salée</h3>
        <p>Chèvre, aubergines grillées, miel, pignons de pin</p>

        <h3>Surprise</h3>
        <p>Dites nous ce que vous n'aimez pas et laissez le Chef faire le reste</p>

        <h3 style="text-align:left;">L’ingrédient supplémentaire au choix est à <span style="float :right">1,50€</span></h3>
        <h3 style="text-align:left;">Galette du jour sur l'ardoise <span style="float :right">9,00€</span></h3>
        <h3 style="text-align:left;">Accompagnement petite salade <span style="float :right">3,50€</span></h3>
        <p>Vinaigrette maison</p>

        <h2>Si aucune de nos suggestions ne vous convient.</h2>
        <p>Vous pouvez créer votre propre galette en piochant dans la liste des ingrédients qui les composent. Nous vous
            conseillons tout de même de ne pas mélanger deux charcuteries ensemble</p>

        <h3 style="text-align:left;">Galette duo<span style="float :right">7,00€</span></h3>
        <h3>Galette duo</h3>
        <p>2 ingrédients au choix</p>

        <h3 style="text-align:left;">Galette trio<span style="float :right">8,50€</span></h3>
        <p>3 ingrédients au choix</p>

        <h3 style="text-align:left;">Galette quatuor<span style="float :right">9,50€</span></h3>
        <p>4 ingrédients au choix</p>

        <hr>
        <h2 style="text-align:center;">Les crêpes de froment</h2>
        <p>Au lait entier</p>

        <h2>Les traditionnelles</h2>
        <h3 style="text-align:left;">Beurre cassonade <span style="float :right">3,50€</span></h3>
        <h3 style="text-align:left;">Beurre cassonade - jus de citron frais <span style="float :right">4,50€</span></h3>
        <h3 style="text-align:left;">Beurre cassonade - cannelle <span style="float :right">4,00€</span></h3>
        <h3 style="text-align:left;">Beurre cassonade - eau de fleur d'oranger <span style="float :right">4,00€</span></h3>
        <h3 style="text-align:left;">Beurre cassonade - eau de rose <span style="float :right">4,00€</span></h3>
        <h3 style="text-align:left;">Miel <span style="float :right">4,50€</span></h3>
        <h3 style="text-align:left;">Crème de marron maison <span style="float :right">4,50€</span></h3>
        <h3 style="text-align:left;">Nutella <span style="float :right">5,00€</span></h3>
        <h3 style="text-align:left;">Chocolat noir maison <span style="float :right">5,00€</span></h3>
        <h3 style="text-align:left;">Caramel beurre salé maison <span style="float :right">5,00€</span></h3>
        <h3 style="text-align:left;">Caramel beurre salé aux épices de pain d'épices <span style="float :right">5,50€</span></h3>
        <h3 style="text-align:left;">Caramel beurre salé fumé maison <span style="float :right">5,50€</span></h3>

        <h3 style="text-align:left;">Nos suggestions <span style="float :right">6,00€</span></h3>


        <h3>Flambée</h3>
        <p>Rhum ou Grand Marnier</p>

        <h3>Banana juju split</h3>
        <p>Crème de marron maison, demi banane poëlée, noix de coco râpée</p>

        <h3>Cheveux d'Ange</h3>
        <p>Carottes confites au miel, à la cassonade et à la cardamome, jus de citron frais</p>

        <h3>Tatin de tata</h3>
        <p>Poires, caramel aux épices de pain d'épices, brisures de sablés bretons</p>

        <h3 style="text-align:left;">Supplément chantilly<span style="float :right">2,00€</span></h3>


</div>
</html>
