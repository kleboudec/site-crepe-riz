<!DOCTYPE html>
<html>
	<head>
	<title>Breizh Ton Riz</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
        <script type="text/javascript" src="../js/menuNav.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	</head>
	<body>
    <?php
    include_once "menuNav.php";
    ?>
    <div id="main">
		<h1 style="text-align:center;">Breizh Ton Riz</h1>
	
			
		<h2 >Mentions légales</h2>
			<h3>Breizh Ton Riz<h3>
				<pre>45 Boulevard Joseph le Bihan,
Perros-Guirec, France
www.breizhtonriz.bzh
chaipasquoimettre@breizhtonriz.bzh
</pre>
			<h3>Credit photographique<h3>
				<pre>Tous droits réservés</pre>
			<h3>Données personnelles</h3>
			<pre>L'Utilisateur s'engage d'ores et déjà à faire des informations contenues sur le site un usage personnel et non commercial.

Nous nous engageons auprès de vous, visiteurs et clients de notre site, à vous garantir la confidentialité des informations personnelles que vous nous fournissez.
En respect des réglementations française et européenne et conformément à la loi Informatique et Libertés en date du 6 janvier 1978, vous disposez d'un droit 
d'accès, de rectification, de modification et de suppression concernant les données qui vous concernent
Par ailleurs, comme la plupart des sites internet, nous collectons également des données relatives à la navigation de nos visiteurs, afin d'obtenir des statistiques 
précises liées à l'utilisation de notre site. Ces données incluent l'adresse IP, le fournisseur d'accès à internet, le navigateur utilisé, ou encore l'heure des visites.

Chaque internaute a la possibilité de désactiver les cookies et d'effacer ceux présents sur son ordinateur, en modifiant les réglages de son navigateur.
    </div>
	</body>
</html>