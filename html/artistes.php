<!DOCTYPE html>
<html lang="fr">

<head>
  <meta name="author" content=" PIERRE Gaëtan">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link rel="stylesheet" href="../css/artistes.css">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <title>Phototèque</title>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
</head>

<body>
<?php
include_once "menuNav.php";
?>
<div id="main">
  <main class="container lex-shrink-0" style="text-align:justify">
    <div class="album py-5 bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrêpeComplète.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Jambon Oeuf Gryuère</h6>
                <p class="card-text">
                  Une crêpe de complète agrémentée d'un oeuf, de jambon de pays ainsi que de gryuère AOP
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrêpeRouléSaumon.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Roulée</h6>
                <p class="card-text">
                  Crêpe roulée, garnie de saumon accompagné de fromage frais
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepeItalie.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Italienne</h6>
                <p class="card-text">
                   Crêpes à l'huile d'olive, ricotta et jambon italien.
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/GalettePoire.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Galette aux poires</h6>
                <p class="card-text">
                  Galette bretonne aux poires, gorgonzola et noix
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepeMakis.webp" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Makis</h6>
                <p class="card-text">
                  Makis de crêpes aux épinards, saumon, avocat et cream cheese
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepeChampignon.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Champignons</h6>
                <p class="card-text">
                  Crêpes à la farine de sarrasin et aux champignons
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepeAsperge.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Asperges</h6>
                <p class="card-text">
                  Crêpes aux asperges, œuf mollet et sauce hollandaise
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepePoulet.jpg" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Poulet</h6>
                <p class="card-text">
                Crêpes poulet et champignons de Paris
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card mb-4 shasow-sm">

              <img src="../src/CrepeChevre.webp" alt="" class="image">
              <div class="card-body">
                <h6 class="font-weight-bold">Crêpe Chèvres</h6>
                <p class="card-text">
                Nids de crêpes chèvre épinards et œuf
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <hr class="featurette-divider">
  </main>
</div>
</body>

</html>
