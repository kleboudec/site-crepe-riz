<!DOCTYPE html>
<html lang="fr">

<head>
    <title>Breizh Ton Riz</title>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/video.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
</head>

<body>
<?php
include_once "menuNav.php";
?>
<div id="main">
    <h1 style="text-align: center">Breizh Ton Riz</h1>
<?php
include_once "video.html";
include_once "Valeur+Equipe.html";
include_once "menu.php";
include_once "menuDuJour.php";
?>
</div>
</body>
</html>
