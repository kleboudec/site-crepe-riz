<!DOCTYPE html>
<html lang="fr">

<head>
  <meta name="author" content="PIERRE Gaëtan">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/video.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="../css/contact.css">
  <link rel="stylesheet" href="../css/newsletter.css">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script src="ressources/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <title>Newsletter</title>
</head>
<?php
include_once "menuNav.php";
?>
<div id="main">
<fieldset> 
<legend>Sign up for Newsletter</legend> 
<form action="newsletter.php" method="post"> 
<p> 
<label>First Name:</label> 
<input type="text" name="name" id="name"/><br><br> 
<label>Email:</label> 
<input type="text" name="email" id="email"/><br> 
<input type="submit" name=submit value="Submit"/> 
</form> 
</fieldset> 
</div>
</html>