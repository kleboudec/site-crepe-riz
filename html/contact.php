<!DOCTYPE html>
<html lang="fr">

<head>
  <meta name="author" content="PIERRE Gaëtan">
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/video.css" media="all" />
    <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
    <script type="text/javascript" src="../js/menuNav.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="../css/contact.css">
  <link rel="stylesheet" href="../bootstrap-4.3.1-dist/css/bootstrap.min.css">
  <script src="ressources/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
  <link rel="icon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <title>Contact</title>
</head>

<body>
<?php
include_once "menuNav.php";
?>
<div id="main">
  <main class="container lex-shrink-0" style="text-align:justify">
    <div class="row">
      <div class="col-md-12">

        <form id="contact" action="" method="post">
          <h3>Formulaire de contact</h3>
          <fieldset>
            <input placeholder="Votre nom" type="text" tabindex="1" required autofocus>
          </fieldset>
          <fieldset>
            <input placeholder="Votre adresse mail" type="email" tabindex="2" required>
          </fieldset>
          <fieldset>
            <input placeholder="Votre téléphone (optionnel)" type="tel" tabindex="3" required>
          </fieldset>
          <fieldset>
            <input placeholder="Votre site internet (optionnel)" type="url" tabindex="4" required>
          </fieldset>
          <fieldset>
            <textarea placeholder="Rentrez votre message ici...." tabindex="5" required></textarea>
          </fieldset>
          <fieldset>
            <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Envoyer</button>
          </fieldset>
        </form>
      </div>
    </div>
    <hr class="featurette-divider">
    <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3">
		<div id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div><script>(function () {
        var setting = {"height":531,"width":837,"zoom":13,"queryString":"45 Boulevard Joseph le Bihan, Perros-Guirec, France","place_id":"ChIJtSg7d-MtEkgRxfaobsWGpII","satellite":false,"centerCoord":[48.81601396815016,-3.4589766999999894],"cid":"0x82a486c56ea8f6c5","lang":"fr","cityUrl":"/france/perros-guirec-12686","cityAnchorText":"Carte de Perros-Guirec, Bretagne, France","id":"map-9cd199b9cc5410cd3b1ad21cab2e54d3","embed_id":"318073"};
        var d = document;
        var s = d.createElement('script');
        s.src = 'https://1map.com/js/script-for-user.js?embed_id=318073';
        s.async = true;
        s.onload = function (e) {
          window.OneMap.initMap(setting)
        };
        var to = d.getElementsByTagName('script')[0];
        to.parentNode.insertBefore(s, to);
      })();</script><a href="https://1map.com/fr/map-embed">1 Map</a></div>
  </main>
</div>
</body>

</html>
