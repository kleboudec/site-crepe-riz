<!DOCTYPE html>
<html>
	<head>
	<title>Breizh Ton Riz</title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
        <script type="text/javascript" src="../js/menuNav.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	</head>
	<body>
    <?php
    include_once "menuNav.php";
    ?>
    <div id="main">
		<h1 style="text-align:center;">Breizh Ton Riz</h1>
	
			
		<h2 >Fil d'actualité</h2>
			<ul>
				<li style="border:2px solid DodgerBlue;font-size:120%;"><h3>La Maison du sarrasin - Bertrand Larcher</h3>
				<pre>
	La Maison du sarrasin, le dernier concept de Bertrand Larcher, est prête à accueillir les Fougerais.
	Elle ouvrira dès la fin du confinement. Sept jours sur sept.
	La Maison du sarrasin va ouvrir ses portes. Bientôt.
					 
	Le projet de Bertrand Larcher, initié fin 2019, aura finalement mis un an à sortir de terre.
	Avec six mois de retard sur le calendrier prévu. La faute à cette satanée covid-19.
	Enfin, il reste un dernier détail : que le Premier ministre lève le confinement.
	« On a hâte. Vraiment. On reste optimistes même si la période est triste », sourit Bertrand
	Larcher venu épauler son équipe à Fougères avant de partir superviser ses affaires au Japon pendant un mois et demi.
				</pre>
				</li>
				
				<li style="border:2px solid DodgerBlue;font-size:120%;"><h3>Atelier de la Crêpe réouvre son restaurant d'application</h3>
				<pre>
	Après 6 mois de fermeture pour cause de Covid-19, l'Atelier de la Crêpe a repris le service et les formations.
	Les inscriptions d'élèves sont complètes jusqu'à la fin de l'année.
					 
	Cela fait maintenant deux ans que l’Atelier de la crêpe a ouvert ses portes au 25 quai Duguay-Trouin à Saint-Malo.
	Si les clients peuvent s’y restaurer en dégustant des galettes et des crêpes de sarrazin, 
	il ne faut pas oublier qu’il s’agit là d’un « restaurant d’application » précise Laure Mariault, 
	qui communique au nom de l’Atelier de la Crêpe. « Il donne l’opportunité à des élèves d’appliquer ce qui leur est enseigné ».
	La rentrée des formations en Certificat de qualification professionnelle (CQP) s’est faite le 14 septembre.
				</pre>
				</li>
			</ul>
    </div>
	</body>
</html>