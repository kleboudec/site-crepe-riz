<!DOCTYPE html>
<html>
	<head>
	<title>Breizh Ton Riz</title>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="../css/menuNav.css" media="all" />
        <script type="text/javascript" src="../js/menuNav.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"
	</head>
	<body>
		<h1 style="text-align:center;">Breizh Ton Riz</h1>

        <?php
        include_once "menuNav.php";
        ?>
        <div id="main">
		<h2 >Résaux sociaux</h2>
			<h3> Retrouvez-nous sur vos résaux sociaux favoris </h3>
			<table>
				<tr>
					<td><a href='https://www.facebook.com/groups/305174256284211'><img src="../src/facebook.png" style="width:250px;height:250px;" alt="imageFacebook"></a></td>
					<td><a href='https://www.facebook.com/groups/305174256284211'>Facebook</a></td>
					<td></td>
				</tr>
				<tr>
					<td><a href='https://twitter.com/feteacrepe?lang=fr'><img src="../src/twitter.jpg" style="width:250px;height:250px;" alt="imageTwitter"></a></td>
					<td><a href='https://twitter.com/feteacrepe?lang=fr'>Twitter</a></td>
							
				</tr>
				<tr>
					<td><a href='https://www.instagram.com/explore/tags/crepes/?hl=fr'><img src="../src/instagram.jpg" style="width:250px;height:250px;" alt="imageInsta"></a></td>
					<td><a href='https://www.instagram.com/explore/tags/crepes/?hl=fr'>Instagram</a></td>
					<td></td>
				</tr>
			</table>
        </div>
	</body>
</html>